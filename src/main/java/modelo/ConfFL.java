package modelo;

public class ConfFL {
	private Integer id;
	private String imageHeaderF;
	private String imageHeaderL;
	private String logoSite;
	private String pint;
	private String twi;
	private String face;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getImageHeaderF() {
		return imageHeaderF;
	}
	public void setImageHeaderF(String imageHeaderF) {
		this.imageHeaderF = imageHeaderF;
	}
	public String getImageHeaderL() {
		return imageHeaderL;
	}
	public void setImageHeaderL(String imageHeaderL) {
		this.imageHeaderL = imageHeaderL;
	}
	public String getLogoSite() {
		return logoSite;
	}
	public void setLogoSite(String logoSite) {
		this.logoSite = logoSite;
	}
	public String getPint() {
		return pint;
	}
	public void setPint(String pint) {
		this.pint = pint;
	}
	public String getTwi() {
		return twi;
	}
	public void setTwi(String twi) {
		this.twi = twi;
	}
	public String getFace() {
		return face;
	}
	public void setFace(String face) {
		this.face = face;
	}
}
