package modelo;

public class ConfHome {
	private Integer id;
	private String imageHeaderH;
	private String imageHeaderH2;
	private String titulo;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getImageHeaderH() {
		return imageHeaderH;
	}
	public void setImageHeaderH(String imageHeaderH) {
		this.imageHeaderH = imageHeaderH;
	}
	public String getImageHeaderH2() {
		return imageHeaderH2;
	}
	public void setImageHeaderH2(String imageHeaderH2) {
		this.imageHeaderH2 = imageHeaderH2;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
}
